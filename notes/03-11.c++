// -----------
// Mon, 11 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename T, typename C = deque<T>>
class stack {
    private:
        C _c;
    public:
        stack () = default;
        stack (const C& c) :
            _c (c)                 // copy constructor
            {}

int main () {
    stack<int>              x;
    stack<int, list<int>>   y;
    vector<int>             v(...);
    stack<int, vector<int>> z(v);
    return 0;}
