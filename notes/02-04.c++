// -----------
// Mon,  4 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        T,  3:30-4:10pm,  Zoom
    Brian
        M,  12-1pm,       GDC 3.302
    Katherine
        T,  9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
int div
n + n/2 + 1
*/

/*
mcl(10, 100)
b = 10
e = 100
m = 51
if 10 < 51
mcl(10, 100) == mcl(51, 100)
10, 20, 40, 80
15, 30, 60
50, 100

mcl(200, 300)
b = 200
e = 300
m = 151
if 200 < 151
doesn't apply
*/

/*
write code that's easy to test

kernel (abstract away I/O)
    Collatz.h
    Collatz.c++

run harness (I/O: stdin and stdout)
    RunCollatz.c++

test harness (I/O: hardwired with unit tests)
    TestCollatz.c++
*/

/*
read, eval, print loop (REPL)
*/

/*
you to need a new file, which the graders don't care about, to send to HackerRank

merge:
    Collatz.h
    Collatz.c++
    RunCollatz.c++

don't forget to remove #include
*/

/*
LAZY cache
cache in response to reading
an array of cycle lengths
you could start with an array of 1 million
trial and error to determine the best size

EAGER cache
cache before reading
you could start with an array of 1 million
trial and error to determine the best size

META cache
cache outside of HackerRank program
it doesn't quite work with cycle lengths
too few could be stored to make a difference

cache max cycle lengths instead
what if we did
mcl(   1, 1000)
mcl(1001, 2000)
mcl(2001, 3000)
mcl(3001, 4000)
...

then
mcl(1500, 3500): without cache: 2000 calculations

with the cache: 1002 calculations
mcl(1500, 2000): 500 calculations
mcl(2001, 3000):   1 look up
mcl(3001, 3500): 500 calculations
*/
