// -----------
// Mon,  4 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
8725
copy and paste RangeIterator, put it inside of Range, rename it iterator
write class Range
    constructor
    begin
    end
*/

Range<int>           x(2, 5);
Range::iterator<int> p = begin(x) // x.begin()

// Java, inner class

class A {
           class B {...}
    static class C {...}}

class Test {
    public static void main (…) {
        A.C c = new A.C();

        A.B b = new A.B();        // no

        A   a = new A();
        A.B b = x.new B();
        A.B b = new A().new B();

// C++, nested class

class A {
    class B {...}} // like Java's A.C

int main () {
    A::B b;
    return 0;}
