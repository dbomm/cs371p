// -----------
// Wed, 20 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

template <typename I1, typename I2>
bool equal (I1 b, I1 e, I2 x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

/*
input iterator
    ++, ==, !=, * (only read from)

output iterator
    ++, * (only write to)

forward iterator
    <input iterator>, * (read/write)

bidirectional iterator
    <forward iterator>, --

random access iterator
    <bidirectional iterator>, [], + int, - int, -, <, >, <=, >=
*/

/*
equal
    input iterator

copy
    input  iterator: source
    output iterator: target

fill
    forward iterator

reverse
    bidirectional iterator

sort
    random access iterator
*/

/*
forward_list (singly-linked)
    forward iterator

list (doubly-linked)
    bidirectional iterator

vector (array)
    random access
*/

void fill (FI b, FI e, T v) {
    while (b != e) {
        *b = v
        ++b;}}
