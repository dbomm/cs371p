// -----------
// Fri,  1 Mar
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

RangeIterator<int> b(2);
RangeIterator<int> e(2);
(b == e) -> b.operator==(e)  // operator== is a method
                             // what I want is this, instead
(b == e) -> operator==(b, e) // operator== is a function

(b == 2) -> operator==(b, 2)
(2 == b) -> operator==(2, b)

template <typename T>
class RangeIterator {
    friend bool operator == (const RangeIterator& lhs, const RangeIterator& rhs);
    private:
        T _v;
    ...

// outside of that

bool operator == (const RangeIterator& lhs, const RangeIterator& rhs) {
    return lhs._v == rhs._v;}

// notion of friend
/*
the entity making the declaration has to be a class
the entity that we're declaring to be a friend
    a class
    a method
    a function

friendship is not symmetric
friendship is not transitive
*/

// equivalent

template <typename T>
class RangeIterator {
    friend bool operator == (const RangeIterator& lhs, const RangeIterator& rhs) {
        return lhs._v == rhs._v;}

    private:
        T _v;

/*
vector
implemented with an array (front-loaded)
cost of adding to the front : O(n)
cost of adding to the middle : O(n)
cost of adding to the back : amortized O(1)
cost of removing from the front : O(n)
cost of removing from the middle : O(n)
cost of removing from the back: O(1)
cost of indexing : O(1)
*/

vector<int>           x(10, 2);
vector<int>::iterator b = begin(x); // same as x.begin()

cout << *b;     // 2
x.push_back(3);
cout << *b;     // undefined!
