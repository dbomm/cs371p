// -----------
// Wed, 27 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF,  12-12:45pm,   GDC 6.302
        TTh, 3-4pm,        Zoom, https://us04web.zoom.us/j/412178924
    Brian
        M,   12-1pm,       GDC 3.302
    Katherine
        T,   9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
refinement
    constructors
    destructor (not finalize)
*/

/*
class defaults to private access
struct defaults to public access
use struct when everything is public
*/

/*
the default is the constructor that can be called with no arguments
*/

template <typename T>
class RangeIterator {
    private:
        T _v;

    public:
        RangeIterator (const T& v) { // T(), default constructor
            _v = v;}                 // T::operator=(T)
                                     // _v.operator=(v)

        // this is better
        RangeIterator (const T& v) : // member initialization list
            _v (v) {}                // T(T), copy constructor

        T operator * () const {      // const makes this invocable on a const object
            return _v;}

        RangeIterator& operator ++ ()
            ++_v;
            return *this;}

        RangeIterator operator ++ (int) {
            RangeIterator cpy(*this);     // RI(RI), copy constructor
            ++*this;
            return cpy;}

        bool operator == (const RangeIterator& rhs) const {
            return _v == rhs._v;}

        bool operator != (const RangeIterator& rhs) const {
            return !(*this == rhs)

int main () {
    RangeIterator<int> b(2);
    cout << b._v;            // no
    int v = *b;
    cout << v;               // 2
    T x;                     // T()

    T y(2, 3);               // T(), 2 arg
    T y(2);                  // T(), 1 arg
    T y();                   // declares a function, y, that takes nothing
                             // and returns a T

    RangeIterator<int> b(2);
    RangeIterator<int> e(2);
    cout << (b == e);        // true
    cout << (b == 2);        // true
    cout << b.operator==(2); // true
    cout << (2 == b);
    cout << 2.

void f (RangeIterator<int> y) {
    ...}

int main () {
    RangeIterator<int> x(2);
    f(x);
    f(RangeIterator<int>(3));
    f(4);
