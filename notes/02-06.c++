// -----------
// Wed,  6 Feb
// -----------

/*
FoCS
    please track and go
    really worth your time

Lab Sessions
    Wed, 6-8pm, GDC 1.302

Office Hours
    Glenn
        MF, 12-12:45pm,   GDC 6.302
        T,  3:30-4:10pm,  Zoom
    Brian
        M,  12-1pm,       GDC 3.302
    Katherine
        T,  9:30-10:30am, GDC 1.302

Piazza
    ask and answer questions
    please be proactive
*/

/*
exceptions
variables
*/

void f (int i2) { // by value
    ...}

int i = 2;
f(i);

int f (...) {
    ...
    if (...)
        throw Tiger(...); // throw always copies
    ...}

void g (...) {
    try {
        ...
        int i = f(...)
        ...
    catch (Mammal e) { // if you catch by value, makes a second copy
        ...}}          // if you catch a parent, you'll get a slice
    ...}

Tiger x(...);
Tiger y = x;
Mammal z(...);
x = z;         // no
z = x;         // slice

int f (...) {
    ...
    if (...)
        Tiger* p = new Tiger(...);
        throw p;                   // throw always copies
    ...}

void g (...) {
    try {
        ...
        int i = f(...)
        ...
    catch (Mammal* e) { // unattractive, because I now have to manage the memory
        ...}}
    ...}

int f (...) {
    ...
    if (...)
        throw Tiger(...); // throw always copies
    ...}

void g (...) {
    try {
        ...
        int i = f(...)
        ...
    catch (Mammal& e) { // this is what we want
        ...}}
    ...}

/*
C++ string == C string

1. C++ string defined an == operator with a C string as an argument
2. convert the C string automatically to a C++ string and then run C++ string's normal == operator
*/

int i = 2;
int v = i;
++v;
cout << i; // 2

/*
two tokens
    *, &

two contexts
    type     annotation
    variable annotation
*/

int  i = 2;
int* p = i;  // no
int* p = &i; // & requires an l-value
++p;         // undefined
cout << p;   // address of int i
cout << *p;  // 2, * requires a pointer
++*p;        // increments int i
cout << i;   // 3

int  i = 2;
int& r = &i; // no
int& r = i;  // r becomes an alias for i
++r;         // increments int i
cout << i;   // 3
