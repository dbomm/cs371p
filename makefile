.DEFAULT_GOAL := all

all:

clean:
	cd examples/c++; make clean
	@echo
	cd examples/java; make clean

config:
	git config -l

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Script.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.c++"                  \
    --include "Assertions.c++"             \
    --include "UnitTests1.c++"             \
    --include "UnitTests2.c++"             \
    --include "UnitTests3.c++"             \
    --include "Coverage1.c++"              \
    --include "Coverage2.c++"              \
    --include "Coverage3.c++"              \
    --include "IsPrime.c++"                \
    --include "IsPrimeT.c++"               \
    --include "Exceptions.c++"             \
    --include "Variables.c++"              \
    --include "Arguments.c++"              \
    --include "Incr.c++"                   \
    --include "IncrT.c++"                  \
    --include "Consts.c++"                 \
    --include "Arrays1.c++"                \
    --include "Equal.c++"                  \
    --include "EqualT.c++"                 \
    --include "Fill.c++"                   \
    --include "FillT.c++"                  \
    --include "Copy.c++"                   \
    --include "CopyT.c++"                  \
    --include "Iterators.c++"              \
    --include "Factorial.c++"              \
    --include "FactorialT.c++"             \
    --include "RangeIterator.c++"          \
    --include "RangeIteratorT.c++"         \
    --include "Range.c++"                  \
    --include "RangeT.c++"                 \
    --include "Iteration.c++"              \
    --include "Types.c++"                  \
    --include "Stack.c++"                  \
    --include "StackT.c++"                 \
    --include "Functions.c++"              \
    --exclude "*"                          \
    ../../examples/c++/ examples/c++/
	@rsync -r -t -u -v --delete            \
    --include "Script.txt"                 \
    --include "Dockerfile"                 \
    --include "Hello.java"                 \
    --include "Assertions.java"            \
    --exclude "*"                          \
    ../../examples/java/ examples/java/
	@rsync -r -t -u -v --delete            \
    --include "Collatz.c++"                \
    --include "Collatz.h"                  \
    --include "RunCollatz.c++"             \
    --include "RunCollatz.in"              \
    --include "RunCollatz.out"             \
    --include "TestCollatz.ctd"            \
    --include "TestCollatz.c++"            \
    --exclude "*"                          \
    ../../projects/c++/collatz/ projects/collatz/
	@rsync -r -t -u -v --delete            \
    --include "TestVoting.ctd"             \
    --exclude "*"                          \
    ../../projects/c++/voting/ projects/voting/
